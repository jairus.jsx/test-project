import React, { Component } from 'react'
import { StyleSheet, Text, View, ImageBackground, Dimensions } from 'react-native'
import { Input, Button } from 'react-native-elements'

import Icon from 'react-native-vector-icons/FontAwesome'

const SCREEN_WIDTH = Dimensions.get('window').width
const SCREEN_HEIGHT = Dimensions.get('window').height

const BG_IMAGE = require('../assets/img/bg_screen1.jpg')

export default class App extends Component {
  constructor(props) {
    super(props)
  }


  render() {
    return (
      <View style={styles.container}>
        <ImageBackground
          source={BG_IMAGE}
          style={styles.bgImage}
        >
          <Text>test</Text>
        </ImageBackground>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  bgImage: {
    flex: 1,
    top: 0,
    left: 0,
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
